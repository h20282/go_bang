#include <cstdint>
#include <iostream>

#include "asio.hpp"

using std::cin;
using std::cout;
using std::endl;

using RoomId = std::string;
using ByteData = std::vector<uint8_t> > ;

// clang-format off
enum HeaderId{
    kCreateRoomReq = 1;
    kCreateRoomRes = 101;
    kJoinRoomReq   = 2;
    kJoinRoomRes   = 102;
}
// clang-format on

class Connection {
    bool Send();
    ByteData Recv();
}

Connection connection;

RoomId CreateRoom() {
    Json msg;
    msg["id"] = kCreateRoomReq;
    connection.Send(msg.dump);
    auto json = Json::parse(Connection.Recv()) RoomId room_id{};
    try {
        if (json.value("result")) {
            room_id = json.value("room_id");
        }
    } catch (std::exception /* e */) {
    }
    return room_id;
}

bool JoinRoom(RoomId room_id) {
    Json msg;
    msg["id"] = kJoinRoomReq;
}

template <typename T>
T GetOp(std::string hint, std::set<T> ops) {
    T op;
    do {
        cout << hint;
        cin >> op;
    } while (ops.find(op) == ops.end());
    return op;
}

int main() {
    auto op =
        GetOp("please choose one\n  1.CreateRoom\n  2.JoinRoom\n", {1, 2});
    if (op==1) {
        
    }

    return 0;
}

